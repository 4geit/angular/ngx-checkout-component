import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxCheckoutComponent } from './ngx-checkout.component';

describe('checkout', () => {
  let component: checkout;
  let fixture: ComponentFixture<checkout>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ checkout ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(checkout);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
