import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxCheckoutComponent } from './ngx-checkout.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgxMaterialModule,
  ],
  declarations: [
    NgxCheckoutComponent
  ],
  exports: [
    NgxCheckoutComponent
  ]
})
export class NgxCheckoutModule { }
