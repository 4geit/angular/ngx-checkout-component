# @4geit/ngx-checkout-component [![npm version](//badge.fury.io/js/@4geit%2Fngx-checkout-component.svg)](//badge.fury.io/js/@4geit%2Fngx-checkout-component)

---

checkout section for marketplace layout

## Installation

1. A recommended way to install ***@4geit/ngx-checkout-component*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-checkout-component) package manager using the following command:

```bash
npm i @4geit/ngx-checkout-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-checkout-component
```

2. You need to import the `NgxCheckoutComponent` component within the module you want it to be. For instance `app.module.ts` as follows:

```js
import { NgxCheckoutComponent } from '@4geit/ngx-checkout-component';
```

And you also need to add the `NgxCheckoutComponent` component with the `@NgModule` decorator as part of the `declarations` list.

```js
@NgModule({
  // ...
  declarations: [
    // ...
    NgxCheckoutComponent,
    // ...
  ],
  // ...
})
export class AppModule { }
```

3. You can also attach the component to a route in your routing setup. To do so, you need to import the `NgxCheckoutComponent` component in the routing file you want to use it. For instance `app-routing.module.ts` as follows:

```js
import { NgxCheckoutComponent } from '@4geit/ngx-checkout-component';
```

And you also need to add the `NgxCheckoutComponent` component within the list of `routes` as follows:

```js
const routes: Routes = [
  // ...
  { path: '**', component: NgxCheckoutComponent }
  // ...
];
```
