import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

import { NgxAuthService } from '@4geit/ngx-auth-service';
import { NgxCartItemsService } from '@4geit/ngx-cart-items-service';

@Component({
  selector: 'ngx-checkout',
  template: require('pug-loader!./ngx-checkout.component.pug')(),
  styleUrls: ['./ngx-checkout.component.scss']
})
export class NgxCheckoutComponent implements OnInit {

  selectedIndex = 0;
  paymentChoice = 'visa';

  constructor(
    private snackBar: MdSnackBar,
    private authService: NgxAuthService,
    private cartItemsService: NgxCartItemsService
  ) { }

  ngOnInit() {
    console.log(this.authService.account);
  }

  updateItem(event, cell, value, row) {
    this.cartItemsService.items[row.$$index][cell] = event.target.value;
    this.cartItemsService.computeItem(row);
  }

  next() {
    this.selectedIndex = this.selectedIndex + 1
    if (this.selectedIndex === 3) {
      this.snackBar.open('Payment succeeded, your order is complete!', 'Hide', {
        duration: 2000
      });
    }
  }

}
